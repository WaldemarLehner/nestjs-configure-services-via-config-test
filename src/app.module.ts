import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { LazyModuleLoader } from "@nestjs/core";
import { AppController } from "./app.controller";
import configuration from "./config/configuration";
import { GreeterModule } from "./services/greeterService/greeter.module";

@Module({
	imports: [
		ConfigModule.forRoot({
			load: [configuration]
		}),
		GreeterModule.forRootAsync(GreeterModule, {
			imports: [ConfigModule],
			inject: [LazyModuleLoader, ConfigService],
			// This is NOT a factory for the Module, but for the Option-Object!
			useFactory: (lazyModuleLoader: LazyModuleLoader, configService: ConfigService) => {
				return { configService, lazyModuleLoader };
			}
		})
	],
	controllers: [AppController],
})
export class AppModule {}

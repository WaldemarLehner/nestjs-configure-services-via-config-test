import { createConfigurableDynamicRootModule } from "@golevelup/nestjs-modules";
import {  Module } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { LazyModuleLoader, ModuleRef } from "@nestjs/core";
import { FirstImplementationModule } from "./firstImplementation/firstImplementation.module";
import { GreeterService } from "./greeter.service";
import { SecondImplementationModule } from "./secondImplementation/secondImplementation.module";

type GreeterModuleOptions = {configService: ConfigService, lazyModuleLoader: LazyModuleLoader};

const GREETER_MODULE_OPTIONS = Symbol("Greeter_Module_Modules");


@Module({
})
export class GreeterModule extends createConfigurableDynamicRootModule<GreeterModule, GreeterModuleOptions>(GREETER_MODULE_OPTIONS, {

	providers: [
		{
			provide: GreeterService,
			inject: [GREETER_MODULE_OPTIONS],
			useFactory: async(options: GreeterModuleOptions) => {
				const ImplementationIndex = options.configService.get("impl") as string;
				let moduleRef : ModuleRef;
				if(ImplementationIndex == "1") {
					moduleRef = await options.lazyModuleLoader.load(() => FirstImplementationModule);
				} else if(ImplementationIndex == "2") {
					moduleRef = await options.lazyModuleLoader.load( () => SecondImplementationModule);
				} else {
					throw new Error("Unknown Implementation Index!");
				}

				return moduleRef.get(GreeterService);
			}
		}
	],
	exports: [
		GreeterService
	]
}) {
}

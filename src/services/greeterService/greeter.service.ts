
export abstract class GreeterService {
	public abstract greet(name: string) : string;
}

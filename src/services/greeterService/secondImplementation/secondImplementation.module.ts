import { Logger, Module } from "@nestjs/common";
import { GreeterService } from "../greeter.service";
import { SecondGreeterService } from "./secondGreeter.service";
import { SecondImplementationDependencyService } from "./secondImplementationDependency.service";

@Module({
	providers: [
		{
			provide: GreeterService,
			useClass: SecondGreeterService
		},
		SecondImplementationDependencyService
	],
	exports: [
		GreeterService
	]
})
export class SecondImplementationModule {
	private logger = new Logger(SecondImplementationModule.name);
	constructor() {
		this.logger.log("::: Module Instantiated");
	}
}

import { Injectable } from "@nestjs/common";

@Injectable()
export class SecondImplementationDependencyService {

	public getGreeting() {
		return "Hello, ";
	}

}

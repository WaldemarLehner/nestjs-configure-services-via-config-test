import { Injectable } from "@nestjs/common";
import { GreeterService } from "../greeter.service";
import { SecondImplementationDependencyService } from "./secondImplementationDependency.service";

@Injectable()
export class SecondGreeterService extends GreeterService {

	constructor(private dependency: SecondImplementationDependencyService) {
		super();
	}

	public greet(name: string): string {
		return `${this.dependency.getGreeting()} ${name}.`;
	}

}

import { Logger, Module } from "@nestjs/common";
import { GreeterService } from "../greeter.service";
import { FirstGreeterService } from "./firstGreeter.service";
import { FirstImplementationDependencyService } from "./firstImplementationDependency.service";

@Module({
	providers: [
		FirstImplementationDependencyService,
		{
			provide: GreeterService,
			useClass: FirstGreeterService
		}
	],
	exports: [
		GreeterService
	]
})
export class FirstImplementationModule {
	private logger = new Logger(FirstImplementationModule.name);
	constructor() {
		this.logger.log("::: Module Instantiated");
	}
}

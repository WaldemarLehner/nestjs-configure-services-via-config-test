import { Injectable } from "@nestjs/common";
import { GreeterService } from "../greeter.service";
import { FirstImplementationDependencyService } from "./firstImplementationDependency.service";

@Injectable()
export class FirstGreeterService extends GreeterService {
	public greet(name: string): string {
		return `${this.dependency.getGreeting()} ${name}!`;
	}
	constructor(private dependency: FirstImplementationDependencyService) {
		super();
	}
}

import { Injectable } from "@nestjs/common";

@Injectable()
export class FirstImplementationDependencyService {

	public getGreeting() {
		return "Hi, ";
	}

}

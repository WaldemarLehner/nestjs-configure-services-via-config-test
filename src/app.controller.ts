import { Controller, Get, Param } from "@nestjs/common";
import { GreeterService } from "./services/greeterService/greeter.service";

@Controller()
export class AppController {
	constructor(private readonly service: GreeterService) {}

	@Get("/:name")
	public getHello(@Param("name") name: string): string {
		return this.service.greet(name);
	}
}

# About

This Repo is an Experiment / Attempt at implementing a "Module"-Chooser.
The idea is to have multiple implementations of a Service that can be switched by simply modifying a value inside the .env-File.

Note that the configuration is not directly fetched via `process.env`, but by using a Service. This means that the decision can be made by using any injectable service.

![](./docs/out.png)

## How does it work?

The Module we want to be able to switch around is imported as an async dynamic Module.

The Module uses `createConfigurableDynamicRootModule` from [@golevelup/nestjs-modules](https://github.com/golevelup/nestjs/tree/master/packages/modules) to create the module.

A good additional resource about `@golevelup/nestjs-modules` can be found at https://dev.to/nestjs/providing-providers-to-dynamic-nestjs-modules-1i6n .


Inside `app.module.ts` we create a Config-Object for the dynamic module using the Factory.

Inside `greeter.module.ts` we access these options and build a **Lazy Module** from the implementation that we want. Each service that should be accessed via the module needs its own factory.
